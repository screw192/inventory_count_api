const mysql = require("mysql2/promise");
let connection = null;

module.exports = {
  connect: async () => {
    connection = await mysql.createConnection({
      host: "127.0.0.1",
      user: "root",
      password: "PassWord666",
      database: "inventory_count",
    });
  },
  getConnection: () => connection
};