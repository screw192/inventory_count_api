const express = require("express");
const cors = require("cors");
const mysqlDB = require("./mysqlDB");
const category = require("./app/category");
const location = require("./app/location");
const inventory = require("./app/inventory");

const app = express();
app.use(express.static("public"));
app.use(express.json());
app.use(cors());

const port = 8000;

app.use("/category", category);
app.use("/location", location);
app.use("/inventory", inventory);

const run = async () => {
  await mysqlDB.connect();

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
};

run().catch(console.error);