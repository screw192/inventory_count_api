const express = require("express");
const mysqlDB = require("../mysqlDB");


const router = express.Router();

router.get("/", async (req, res) => {
  const [location_items] = await mysqlDB.getConnection().query(
      "SELECT ?? FROM location",
      [["id", "location_name"]]
  );
  res.send(location_items);
});

router.get("/:id", async (req, res) => {
  const [result] = await mysqlDB.getConnection().query(
      "SELECT * FROM location WHERE id = ?",
      [req.params.id]
  );

  const location_item = result[0];
  res.send(location_item);
});

router.post("/", async (req, res) => {
  const location_item = req.body;
  location_item.dateAdded = new Date().toISOString();

  Object.keys(location_item).forEach(key => {
    if (location_item[key] === "") {
      location_item[key] = null;
    }
  });

  const [result] = await mysqlDB.getConnection().query(
      "INSERT INTO location SET ?",
      location_item
  );

  res.send({...location_item, id: result.insertId});
});

router.delete("/:id", async (req, res) => {
  try {
    const [result] = await mysqlDB.getConnection().query(
        "DELETE FROM location WHERE id = ?",
        [req.params.id]
    );

    res.send(result);
  } catch (e) {
    res.send(e.message);
  }
});

router.put("/:id", async (req, res) => {
  const location_update = req.body;

  Object.keys(location_update).forEach(key => {
    if (location_update[key] === "") {
      location_update[key] = null;
    }
  });

  await mysqlDB.getConnection().query(
      "UPDATE location SET ? WHERE id = ?",
      [location_update, req.params.id]
  );

  const [result] = await mysqlDB.getConnection().query(
      "SELECT * FROM location WHERE id = ?",
      [req.params.id]
  );

  res.send(result);
});

module.exports = router;