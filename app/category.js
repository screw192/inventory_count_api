const express = require("express");
const mysqlDB = require("../mysqlDB");


const router = express.Router();

router.get("/", async (req, res) => {
  const [category_items] = await mysqlDB.getConnection().query(
      "SELECT ?? FROM category",
      [["id", "category_name"]]
  );
  res.send(category_items);
});

router.get("/:id", async (req, res) => {
  const [result] = await mysqlDB.getConnection().query(
      "SELECT * FROM category WHERE id = ?",
      [req.params.id]
  );

  const category_item = result[0];
  res.send(category_item);
});

router.post("/", async (req, res) => {
  const category_item = req.body;
  category_item.dateAdded = new Date().toISOString();

  Object.keys(category_item).forEach(key => {
    if (category_item[key] === "") {
      category_item[key] = null;
    }
  });

  const [result] = await mysqlDB.getConnection().query(
      "INSERT INTO category SET ?",
      category_item
  );

  res.send({...category_item, id: result.insertId});
});

router.delete("/:id", async (req, res) => {
  try {
    const [result] = await mysqlDB.getConnection().query(
        "DELETE FROM category WHERE id = ?",
        [req.params.id]
    );

    res.send(result);
  } catch (e) {
    res.send(e.message);
  }
});

router.put("/:id", async (req, res) => {
  const category_update = req.body;

  Object.keys(category_update).forEach(key => {
    if (category_update[key] === "") {
      category_update[key] = null;
    }
  });

  await mysqlDB.getConnection().query(
      "UPDATE category SET ? WHERE id = ?",
      [category_update, req.params.id]
  );

  const [result] = await mysqlDB.getConnection().query(
      "SELECT * FROM category WHERE id = ?",
      [req.params.id]
  );

  res.send(result);
});

module.exports = router;