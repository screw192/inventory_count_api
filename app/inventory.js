const express = require("express");
const mysqlDB = require("../mysqlDB");


const router = express.Router();

router.get("/", async (req, res) => {
  const [inventory_items] = await mysqlDB.getConnection().query(
      "SELECT ?? FROM inventory",
      [["id", "category_id", "location_id",  "inventory_name"]]
  );
  res.send(inventory_items);
});

router.get("/:id", async (req, res) => {
  const [result] = await mysqlDB.getConnection().query(
      "SELECT * FROM inventory WHERE id = ?",
      [req.params.id]
  );

  const inventory_item = result[0];
  res.send(inventory_item);
});

router.post("/", async (req, res) => {
  const inventory_item = req.body;
  inventory_item.dateAdded = new Date().toISOString();

  Object.keys(inventory_item).forEach(key => {
    if (inventory_item[key] === "") {
      inventory_item[key] = null;
    }
  });

  const [result] = await mysqlDB.getConnection().query(
      "INSERT INTO inventory SET ?",
      inventory_item
  );

  res.send({...inventory_item, id: result.insertId});
});

router.delete("/:id", async (req, res) => {
  try {
    const [result] = await mysqlDB.getConnection().query(
        "DELETE FROM inventory WHERE id = ?",
        [req.params.id]
    );

    res.send(result);
  } catch (e) {
    res.send(e.message);
  }
});

router.put("/:id", async (req, res) => {
  const inventory_update = req.body;

  Object.keys(inventory_update).forEach(key => {
    if (inventory_update[key] === "") {
      inventory_update[key] = null;
    }
  });

  await mysqlDB.getConnection().query(
      "UPDATE inventory SET ? WHERE id = ?",
      [inventory_update, req.params.id]
  );

  const [result] = await mysqlDB.getConnection().query(
      "SELECT * FROM inventory WHERE id = ?",
      [req.params.id]
  );

  res.send(result);
});

module.exports = router;